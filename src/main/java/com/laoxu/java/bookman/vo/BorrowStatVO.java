package com.laoxu.java.bookman.vo;

import lombok.Data;

import java.util.List;

/**
 * @Description: 借阅统计
 * @Author laoxu
 * @Date 2020/2/4 14:05
 **/
@Data
public class BorrowStatVO {
    private String name;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Integer> getData() {
        return data;
    }

    public void setData(List<Integer> data) {
        this.data = data;
    }

    private List<Integer> data;
}
