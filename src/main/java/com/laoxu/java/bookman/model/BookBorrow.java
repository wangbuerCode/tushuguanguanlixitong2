package com.laoxu.java.bookman.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 借阅管理
 * @Author laoxu
 * @Date 2020/1/12 22:37
 **/
@Data
public class BookBorrow extends BaseModel {
    private String readerCode;
    private String bookIsbn;
    private Date borrowDate;
    private Date returnDate;
    // 实际归还日期
    private Date realReturnDate;
    private Integer borrowDays;
    // 续借天数
    private Integer reBorrowDays;
    // 借阅状态 0未还；1已还；2逾期
    private Integer borrowStatus;

    public String getReaderCode() {
        return readerCode;
    }

    public void setReaderCode(String readerCode) {
        this.readerCode = readerCode;
    }

    public String getBookIsbn() {
        return bookIsbn;
    }

    public void setBookIsbn(String bookIsbn) {
        this.bookIsbn = bookIsbn;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public Date getRealReturnDate() {
        return realReturnDate;
    }

    public void setRealReturnDate(Date realReturnDate) {
        this.realReturnDate = realReturnDate;
    }

    public Integer getBorrowDays() {
        return borrowDays;
    }

    public void setBorrowDays(Integer borrowDays) {
        this.borrowDays = borrowDays;
    }

    public Integer getReBorrowDays() {
        return reBorrowDays;
    }

    public void setReBorrowDays(Integer reBorrowDays) {
        this.reBorrowDays = reBorrowDays;
    }

    public Integer getBorrowStatus() {
        return borrowStatus;
    }

    public void setBorrowStatus(Integer borrowStatus) {
        this.borrowStatus = borrowStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    private String remark;

}
