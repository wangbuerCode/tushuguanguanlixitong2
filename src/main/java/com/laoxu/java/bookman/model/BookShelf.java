package com.laoxu.java.bookman.model;


import lombok.Data;

/**
 * @Description: 书架
 * @Author laoxu
 * @Date 2019/12/29 15:26
 **/
@Data
public class BookShelf extends BaseModel {
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String code;
    private String name;
}
