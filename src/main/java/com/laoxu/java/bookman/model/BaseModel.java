package com.laoxu.java.bookman.model;

import lombok.Data;

import java.util.Date;

/**
 * @Description: 基本模型字段
 * @Author laoxu
 * @Date 2019/12/29 15:24
 **/
@Data
public abstract class BaseModel {
    private Long id;
    // 创建时间
    private Date createTime;
    // 创建人
    private String creater;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdater() {
        return updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }

    // 修改时间
    private Date updateTime;
    // 修改人
    private String updater;
}
