package com.laoxu.java.bookman.model;

import lombok.Data;

/**
 * @Description: 用户
 * @Author laoxu
 * @Date 2020/2/3 14:38
 **/
@Data
public class User {
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String password;
}
